# Wikibase for the _Connecting Collections_ Projects

_Connecting Collections_ is a
[Linked Open Data](https://en.wikipedia.org/wiki/Linked_data) run by Annika
Hendriksen. Its aim is to use Linked Data technologies to connect and reconcile the
various Naturalis collection databases with each other and with the semantic web at large.
